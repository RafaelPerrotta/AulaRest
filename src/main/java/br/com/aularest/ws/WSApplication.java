package br.com.aularest.ws;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/rest/*")
public class WSApplication extends Application{

}
